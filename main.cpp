#include "mainwindow.h"
#include <QListView>
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QStringList stringList;
    stringList.append(QString("one"));
    stringList.append(QString("two"));
    stringList.append(QString("three"));
    stringList.append(QString("four"));
    stringList.append(QString("five"));
    stringList.append(QString("six"));
    stringList.append(QString("seven"));
    stringList.append(QString("eight"));
    stringList.append(QString("nine"));
    stringList.append(QString("ten"));

    MainWindow mainWindow(stringList);
    mainWindow.show();

    return a.exec();
}
