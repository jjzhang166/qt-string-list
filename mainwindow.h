#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "stringlistmodel.h"
#include <QListView>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(const QStringList &stringList, QWidget *parent = 0);
    QListView* getListView();
    ~MainWindow();

public slots:
    void onAddString();
    void onRemoveString();
    void rowChanged(QModelIndex index);

private:
    Ui::MainWindow *ui;
    StringListModel m_model;
    int m_selectedRow;
};

#endif // MAINWINDOW_H
