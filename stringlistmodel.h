#ifndef STRINGLISTMODEL_H
#define STRINGLISTMODEL_H

#include <QStringListModel>

class StringListModel : public QStringListModel
{
public:
    StringListModel(const QStringList &stringList, QObject *parent=nullptr);
    int rowCount(const QModelIndex &index = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole) const;
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role);
    bool insertRows(int position, int rows, const QModelIndex &parent = QModelIndex());
    bool removeRows(int position, int rows, const QModelIndex &parent = QModelIndex());
private:
    QStringList m_stringList;
};

#endif // STRINGLISTMODEL_H
